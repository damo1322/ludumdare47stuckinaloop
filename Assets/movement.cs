﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine;
using UnityEngine.UI;



public class movement : MonoBehaviour
{

    public float speed = 0;

    private Rigidbody rb;

    private float movementX;
    private float movementY;
    private List<List<Vector3>> memory; 
    bool record = true;
    bool replay = false;
    int replayI = 0;
    int listNumber = 0;
    Vector3 originalPos;
    Dictionary<int, List<Vector3>> listDictionary = new Dictionary<int, List<Vector3>>();
    // dictionary of generated gameObjects?
    public Dictionary<int, GameObject> objectDictionary = new Dictionary<int, GameObject>();
    Dictionary<int, List<Quaternion>> quatDictionary = new Dictionary<int, List<Quaternion>>();
    public GameObject preFabGhostSphere;
    public Text textToUpdate;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        // memory.Add(new List<Vector3>());
        List<Vector3> first = new List<Vector3>();
        List<Quaternion> firstQuat = new List<Quaternion>();
        listDictionary.Add(listNumber, first);
        quatDictionary.Add(listNumber, firstQuat);
        originalPos = transform.position;
    }

    private void OnMove(InputValue movementValue)
    {
        Vector2 movementVector = movementValue.Get<Vector2>();

        movementX = movementVector.x;
        movementY = movementVector.y;
    } 



    public void nextCar() {
            print("Next Car!");
            replay = true;
            GameObject GhostSphere = Instantiate(preFabGhostSphere) as GameObject;
            objectDictionary.Add(listNumber, GhostSphere);
            // transform.position = originalPos;
            replayI = 0;
            List<Vector3> nextVector = new List<Vector3>();
            List<Quaternion> nextQuat = new List<Quaternion>();
            listNumber ++;
            listDictionary.Add(listNumber, nextVector);
            quatDictionary.Add(listNumber, nextQuat);
            textToUpdate.text = "Other idiots driving: " + listNumber;

    }

    private void Update(){
            if (Input.GetKey ("escape")) {
                Application.Quit();
            }
    }
    private void FixedUpdate()
    {
              
        listDictionary[listNumber].Add(transform.position);
        quatDictionary[listNumber].Add(transform.rotation);
        
    if(replay) {
        for (int i = 0; i < objectDictionary.Count; i++) {
            // uf check replay too big, just set to last position...
            print("Current object" + i);
            int replayCap = replayI;
            print("CurrentListDictCount: " + listDictionary[i].Count + " current replayI: " + replayI);
            if(listDictionary[i].Count < (replayI )) {
                print("Capping");
                replayCap = listDictionary[i].Count - 1;
            }
            print("CurrentListDictCount: " + listDictionary[i].Count + " current replayI: " + replayI + " REplay cap: " + replayCap);

            objectDictionary[i].transform.position = listDictionary[i][replayCap];
            objectDictionary[i].transform.rotation = quatDictionary[i][replayCap];
            
        }
        replayI ++;
        }
    }

    
}
